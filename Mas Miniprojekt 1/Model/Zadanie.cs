﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mas_Miniprojekt_1
{

    public enum Status
    {
        InProgress,
        Done
    }

    class Zadanie
    {


        public string Tytul { get; }
        public string Opis { get; }
        public Status Status { get; }

        public Zadanie(string tytul, string opis)
        {
            Tytul = tytul;
            Opis = opis;
            Status = Status.InProgress;
        }
    }
}
