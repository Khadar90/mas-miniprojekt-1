﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Mas_Miniprojekt_1.Model
{
    class Postac
    {
        public Konto Konto { get; }
        public string Nazwa { get; }
        public Klasa Klasa { get; }
        public Rasa Rasa { get; }
        public long Exp { get; }
        public int Level => (int) (5*Math.Sqrt(Exp));
        public List<int> Przedmioty;
        public Postac Mentor { get; private set; }
        public List<Postac> Uczniowie { get; }
        public Dictionary<string, Zadanie> Zadania { get; }
        public List<Znajomy> Znajomi { get; }

        public Postac(Konto konto, string nazwa, Klasa klasa, Rasa rasa)
        {
            if (konto == null)
            {
                throw new Exception("Calosc nie istnieje.");
            }

            konto.DodajPostac(this);

            Konto = konto;
            Nazwa = nazwa;
            Klasa = klasa;
            Rasa = rasa;
            Przedmioty = new List<int>();
            Uczniowie = new List<Postac>();
            Zadania = new Dictionary<string, Zadanie>();
            Znajomi = new List<Znajomy>();
        }

        public void UstawUcznia(Postac postac)
        {
            if (postac.Uczniowie.Contains(postac))
            {
                Console.Out.WriteLine("Ta postać juz sie szkoli u tego mentora");
            }
            else if (postac.Mentor != null)
            {
                Console.Out.WriteLine("Ta postać ma juz mentora");
            }
            else
            {
                Uczniowie.Add(postac);
                postac.UstawMentora(this);
            }
        }

        public void DodajPrzedmiot(int id)
        {
            Przedmioty.Add(id);
        }

        public List<Przedmiot> GetPrzedmioty
            => Przedmioty.Select(id => Przedmiot.Ekstensja.First(x => x.Id == id)).ToList();

        public void DodajZadanie(Zadanie zadanie)
        {
            if (!Zadania.ContainsKey(zadanie.Tytul))
            {
                Zadania.Add(zadanie.Tytul, zadanie);
            }
            else
            {
                Console.Out.WriteLine("Ta postac ma juz do zadanie");
            }
        }

        public Zadanie GetZadanie(string tytul)
        {
            if (Zadania.ContainsKey(tytul))
            {
                return Zadania[tytul];
            }
            throw new Exception("Nie odnaleziono tego zadania.");
        }

        public Zadanie GetZadanie(Zadanie zadanie)
        {
            return GetZadanie(zadanie.Tytul);
        }

        public void DodajZnajomego(Znajomy znajomy)
        {
            Znajomi.Add(znajomy);
        }

        public void DodajZnajomego(Postac postac, string grupa)
        {
            Znajomi.Add(new Znajomy(this, postac, grupa, DateTime.Now));
        }

        public void UsunZadanie(string zadanie)
        {
            if (Zadania.ContainsKey(zadanie))
            {
                Zadania.Remove(zadanie);
            }
        }

        public void UsunZadanie(Zadanie zadanie)
        {
            UsunZadanie(zadanie.Tytul);
        }

        public void UsunZnajomego(Postac postac)
        {
            Znajomi.Where(x => x.Znajomy1 == postac || x.Znajomy2 == postac).ToList().ForEach(x => x.Usun());
        }

        public void UsunUcznia(Postac p)
        {
            if (Uczniowie.Contains(p))
            {
                Uczniowie.Remove(p);
            }
        }

        private void UstawMentora(Postac postac)
        {
            Mentor = postac;
        }
    }
}
