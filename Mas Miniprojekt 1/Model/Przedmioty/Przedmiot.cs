﻿

using System.Collections.Generic;

namespace Mas_Miniprojekt_1.Model
{
    abstract class Przedmiot
    {
        public static List<Przedmiot> Ekstensja = new List<Przedmiot>(); 

        public int Id { get; }
        public string Nazwa { get; }
        public string Opis { get; }

        protected Przedmiot(int id,string nazwa, string opis)
        {
            Id = id;
            Nazwa = nazwa;
            Opis = opis;

            Ekstensja.Add(this);
        }
    }
}
