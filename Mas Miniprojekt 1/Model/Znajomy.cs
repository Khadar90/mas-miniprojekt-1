﻿using System;


namespace Mas_Miniprojekt_1.Model
{
    class Znajomy
    {
        public Postac Znajomy1 { get; }
        public Postac Znajomy2 { get; }
        public string Grupa { get; }
        public DateTime ZnajomiOd { get; }

        public Znajomy(Postac znajomy1, Postac znajomy2, string grupa, DateTime znajomiOd)
        {
            Znajomy1 = znajomy1;
            Znajomy2 = znajomy2;
            Grupa = grupa;
            ZnajomiOd = znajomiOd;

            znajomy2.DodajZnajomego(this);
        }

        //CLR Usunie cały ten obiekt po skasowaniu wszystkich referencji.
        public void Usun()
        {
            Znajomy1.Znajomi.Remove(this);
            Znajomy2.Znajomi.Remove(this);
        }
    }
}
