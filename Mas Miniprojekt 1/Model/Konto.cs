﻿using System;
using System.Collections.Generic;

namespace Mas_Miniprojekt_1.Model
{
    class Konto
    {
        public int Id { get; }
        public string Email { get; }
        public string Login { get; }
        public string Password { get; }
        public Adres Adres { get; }
        public List<KartaKredytowa> KartyKredytowe { get; }
        public List<Postac> Postacie { get; }

        public Konto(int id, string email, string login, string password, Adres adres)
        {
            Id = id;
            Email = email;
            Login = login;
            Password = password;
            Adres = adres;
            KartyKredytowe = new List<KartaKredytowa>();
            Postacie = new List<Postac>();
        }


        public Konto(int id, string email, string login, string password, Adres adres,
            List<KartaKredytowa> kartyKredytowe) : this(id, email, login, password, adres)
        {
            KartyKredytowe = kartyKredytowe;
        }

        public void DodajPostac(string nazwa,Klasa klasa,Rasa rasa)
        {
            DodajPostac(new Postac(this, nazwa, klasa, rasa));
        }

        public void DodajPostac(Postac postac)
        {
            if (postac.Konto != null)
            {
                if (postac.Konto != this)
                {
                    throw new Exception("Kompozycja nie moze byc wspoldzielona");
                }
            }

            if (!Postacie.Contains(postac))
            {
                Postacie.Add(postac);
            }
        }

        public void UsunPostac(Postac postac)
        {
            Postacie.Remove(postac);
        }
    }

    internal class KartaKredytowa
    {
        public string Typ { get; }
        public long Numer { get; }
        public int Cvv { get; }

        public KartaKredytowa(string typ, long numer, int cvv)
        {
            Typ = typ;
            Numer = numer;
            Cvv = cvv;
        }
    }

    internal class Adres
    {
        public string Miasto { get; }
        public string KodPocztowy { get; }
        public int NumerBloku { get; }
        public int? NumerMieszkania { get; }

        public Adres(string miasto, string kodPocztowy, int numerBloku, int? numerMieszkania)
        {
            Miasto = miasto;
            KodPocztowy = kodPocztowy;
            NumerBloku = numerBloku;
            NumerMieszkania = numerMieszkania;
        }

        public Adres(string miasto, string kodPocztowy, int numerBloku) : this(miasto, kodPocztowy, numerBloku, null)
        {
        }
    }
}
