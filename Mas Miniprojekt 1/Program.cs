﻿using System;
using Mas_Miniprojekt_1.Model;
using Mas_Miniprojekt_1.Model.Przedmioty;

namespace Mas_Miniprojekt_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Konto konto1 = new Konto(1,"Test@Gmail.com","Admin","Admin",new Adres("Warszawa","00-000",1,1));
            Konto konto2 = new Konto(2, "Test@Gmail.com", "Admin", "Admin", new Adres("Warszawa", "00-000", 1, 1));

            konto1.DodajPostac("test",null,null);
            konto1.DodajPostac("test2",null,null);
            konto1.DodajPostac("test3", null, null);



            Miecz yklord = new Miecz(1,"Yklord","blabla..");
            Miecz fapfap = new Miecz(2,"Fapfap","fapfap");

            Postac p1 = konto1.Postacie[0];
            Postac p2 = konto1.Postacie[1];
            Postac p3 = konto1.Postacie[2];

        //    konto2.DodajPostac(p1);

            p1.DodajPrzedmiot(yklord.Id);
            p1.DodajPrzedmiot(yklord.Id);
            p1.DodajPrzedmiot(fapfap.Id);
            p1.DodajPrzedmiot(fapfap.Id);

            p1.DodajZnajomego(p2,"testowa");
            p2.DodajZnajomego(p3,"testowa");
            
            Console.Out.WriteLine(p2.Znajomi.Count);
            p1.UsunZnajomego(p2);
            Console.Out.WriteLine(p2.Znajomi.Count);


        }
    }
}

